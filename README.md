    .
    ├── age_prediction
    │   ├── inference.py
    │   ├── input_pipeline.py
    │   ├── model.py
    │   ├── README.md
    │   └── training.py
    ├── bias_sampling_age.py
    ├── harmonization
    │   ├── inference.py
    │   ├── input_pipeline.py
    │   ├── model_architectures.py
    │   ├── README.md
    │   └── training.py
    ├── iqms.py
    └── tissue_volumes.py